import 'package:flutter/material.dart';
import 'main.dart';
import 'mainPage.dart';

class Login extends StatefulWidget {
  @override
  _Login createState() => _Login();
}

class _Login extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 189, 189, 189),
      appBar: AppBar(
        backgroundColor: Colors.yellow.shade600,
        title: const Text("Buu Login",style: TextStyle(color: Color.fromARGB(255, 14, 12, 12)),),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 50.0),
              child: Center(
                child: Container(
                    width: 250,
                    height: 100,
                    child: Image.network('img/Buu-logo11.png')),
              ),
            ),
            
            const Padding(
              padding: EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              child: TextField(
                
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color.fromARGB(255, 0, 0, 0))
                    ),
                    counterText: 'Username',
                    ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              child: TextField(
               
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color.fromARGB(255, 0, 0, 0))
                    ),
                    counterText: 'Password',
                    ),
              ),
            ),
            const Padding(padding: EdgeInsets.all(20.0)),
            Container(
              height: 50,
              width: 300,
              decoration: BoxDecoration(
                  color: Color.fromARGB(255, 218, 183, 88), borderRadius: BorderRadius.circular(20)),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                
                  elevation: 20,
                ),
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => MyApp()));
                },
                child: const Text(
                  'ตรวจสอบ',
                  style: TextStyle(color: Color.fromARGB(255, 0, 0, 0), fontSize: 25),
                ),
              ),
            ),
            
           
          ],
        ),
      ),
    );
  }
}
