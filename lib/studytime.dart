import 'package:flutter/material.dart';
import 'Drawer.dart';
import 'main.dart';
import 'mainPage.dart';

class Study extends StatefulWidget {
  @override
  _Study createState() => _Study();
}

class _Study extends State<Study> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 189, 189, 189),
      appBar: AppBar(
        toolbarHeight: 110,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.menu,
                color: Color.fromARGB(255, 0, 0, 0),
              ),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        title: Container(
            child: Row(
          children: [
            Column(
              children: [
                Container(
                  margin: const EdgeInsets.all(5),
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage('img/Buu-logo-black.png'),
                    ),
                  ),
                )
              ],
            ),
            Column(
              children: [
                Text(
                  "มหาวิทยาลัยบูรพา",
                  style: TextStyle(fontSize: 20, color: Colors.black),
                ),
                Text(
                  "Burapha University",
                  style: TextStyle(fontSize: 15, color: Colors.black),
                ),
              ],
            ),
          ],
        )),
        backgroundColor: Colors.yellow,
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
              icon: Text(
                "ไทย",
                style: TextStyle(fontSize: 14, color: Colors.black),
              ),
              // color: Colors.black,
              onPressed: () {}),
        ],
      ),
      drawer: Drawerme(),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 50.0),
              child: Center(
                child: Container(),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text(
                'ตารางเรียน',
                style: TextStyle(fontSize: 25),
                
              ),
              
            ),
            Image.network('img/Studytime.png'),
           
          ],
        ),
      ),
    );
  }
}
