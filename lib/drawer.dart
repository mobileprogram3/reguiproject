import 'package:flutter/material.dart';
import 'package:flutter_application_1/historyme.dart';
import 'package:flutter_application_1/login.dart';
import 'package:flutter_application_1/search.dart';
import 'package:flutter_application_1/studytime.dart';
import 'package:sidebarx/sidebarx.dart';

import 'main.dart';
import 'mainPage.dart';

class Drawerme extends StatelessWidget {
  Drawerme({Key? key}) : super(key: key);

  final _controller = SidebarXController(selectedIndex: 0, extended: true);
  final _key = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return SidebarX(
      controller: _controller,
      theme: SidebarXTheme(
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: canvasColor,
          borderRadius: BorderRadius.circular(20),
        ),
        hoverColor: scaffoldBackgroundColor,
        textStyle: TextStyle(color: Colors.white.withOpacity(0.7)),
        selectedTextStyle: const TextStyle(color: Colors.white),
        itemTextPadding: const EdgeInsets.only(left: 30),
        selectedItemTextPadding: const EdgeInsets.only(left: 30),
        itemDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: canvasColor),
        ),
        selectedItemDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: actionColor.withOpacity(0.37),
          ),
          gradient: const LinearGradient(
            colors: [accentCanvasColor, canvasColor],
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.28),
              blurRadius: 30,
            )
          ],
        ),
        iconTheme: IconThemeData(
          color: Colors.white.withOpacity(0.7),
          size: 20,
        ),
        selectedIconTheme: const IconThemeData(
          color: Colors.white,
          size: 20,
        ),
      ),
      extendedTheme: const SidebarXTheme(
        width: 200,
        decoration: BoxDecoration(
          color: canvasColor,
        ),
      ),
      footerDivider: divider,
      headerBuilder: (context, extended) {
        return SizedBox(
          height: 150,
          child: ListView(
            children: [
              Image(
                image: NetworkImage(
                  "img/profile.jpg"),
                width: 70,
                height: 70,
              ),
              Text(
                " Aketanawat Kotyota",
                style: TextStyle(
                    fontSize: 20, color: Color.fromARGB(255, 255, 255, 255)),
              ),
              Text(
                "63160230@go.buu.ac.th",
                style: TextStyle(
                    fontSize: 15, color: Color.fromARGB(255, 255, 255, 255)),
              ),
            ],
          ),
        );
      },
      items: [
        SidebarXItem(
          icon: Icons.home,
          label: 'หน้าหลัก',
          onTap: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => MyApp()),
            );
          },
        ),
        SidebarXItem(
          icon: Icons.search,
          label: 'ค้นหานิสิต',
          onTap: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => Search()),
            );
          },
        ),
        SidebarXItem(
          icon: Icons.table_rows_rounded,
          label: 'ตารางเรียน',
          onTap: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => Study()),
            );
          },
        ),
        SidebarXItem(
          icon: Icons.account_box,
          label: 'ประวัตินิสิต',
          onTap: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => History()),
            );
          },
        ),
        SidebarXItem(
          icon: Icons.logout,
          label: 'ออกจากระบบ',
          onTap: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => Login()),
            );
          }
        ),
      ],
    );
  }
}
