import 'package:flutter/material.dart';
import 'package:babstrap_settings_screen/babstrap_settings_screen.dart';
import 'package:flutter_application_1/drawer.dart';
import 'package:simple_grid/simple_grid.dart';

void main() {
  runApp(History());
}

class History extends StatefulWidget {
  @override
  _History createState() => _History();
}

class _History extends State<History> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white.withOpacity(.94),
        appBar: AppBar(
          toolbarHeight: 110,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(
                  Icons.menu,
                  color: Color.fromARGB(255, 0, 0, 0),
                ),
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
              );
            },
          ),
          title: Container(
              child: Row(
            children: [
              Column(
                children: [
                  Container(
                    margin: const EdgeInsets.all(5),
                    width: 45,
                    height: 45,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage('img/Buu-logo-black.png'),
                      ),
                    ),
                  )
                ],
              ),
              Column(
                children: [
                  Text(
                    "มหาวิทยาลัยบูรพา",
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  ),
                  Text(
                    "Burapha University",
                    style: TextStyle(fontSize: 15, color: Colors.black),
                  ),
                ],
              ),
            ],
          )),
          backgroundColor: Colors.yellow,
          elevation: 0.0,
          actions: <Widget>[
            IconButton(
                icon: Text(
                  "ไทย",
                  style: TextStyle(fontSize: 14, color: Colors.black),
                ),
                // color: Colors.black,
                onPressed: () {}),
          ],
        ),
        drawer: Drawerme(),
        body: Padding(
          padding: const EdgeInsets.all(10),
          child: ListView(
            children: [
              
              Text("ประวัติส่วนตัว", style: TextStyle(fontSize: 22)),
              SimpleUserCard(
                userName:
                    "นาย เอกธนวัฒน์ โคตรโยธา                            รหัสนิสิต: 63160230",
                userProfilePic: NetworkImage(
                    "img/profile.jpg"),
              ),
              SpGrid(
                  alignment: WrapAlignment.center,
                  width: MediaQuery.of(context).size.width,
                  spacing: 10,
                  runSpacing: 10,
                  children: [
                    SpGridItem(
                      xs: 12,
                      md: 12,
                      lg: 12,
                      order: SpOrder(sm: 1, xs: 1),
                      child: Center(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Container(
                            child: DataTable(
                              columnSpacing: 1.0,
                              columns: [
                                DataColumn(
                                    label: Text('ประวัติ',
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold))),
                                DataColumn(
                                    label: Text('',
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold))),
                              ],
                              rows: [
                                DataRow(cells: [
                                  DataCell(Text('รหัสประจำตัว:')),
                                  DataCell(Text('63160230')),
                                ]),
                                DataRow(cells: [
                                  DataCell(Text('เลขบัตรประชาชน:')),
                                  DataCell(Text('12799xxxxxxxx')),
                                ]),
                                DataRow(cells: [
                                  DataCell(Text('ชื่อ:')),
                                  DataCell(Text('นายเอกธนวัฒน์ โคตรโยธา')),
                                ]),
                                DataRow(cells: [
                                  DataCell(Text('ชื่อภาษาอังกฤษ:')),
                                  DataCell(Text('MR.AKETANAWAT KOTYOTA')),
                                ]),
                                DataRow(cells: [
                                  DataCell(Text('คณะ:')),
                                  DataCell(Text('วิทยาการสารสนเทศ')),
                                ]),
                                DataRow(cells: [
                                  DataCell(Text('วิทยาเขต:')),
                                  DataCell(Text('บางแสน')),
                                ]),
                                DataRow(cells: [
                                  DataCell(Text('หลักสูตร:')),
                                  DataCell(Text(
                                      'วิทยาการคอมพิวเตอร์ \nปรับปรุง 59 - ป.ตรี 4 ปี ปกติ')),
                                ]),
                                DataRow(cells: [
                                  DataCell(Text('ระดับการศึกษา:')),
                                  DataCell(Text('ปริญญาตรี')),
                                ]),
                                DataRow(cells: [
                                  DataCell(Text('วิธีรับเข้า:')),
                                  DataCell(Text('รับตรงทั่วประเทศ')),
                                ]),
                                DataRow(cells: [
                                  DataCell(Text('วุฒิก่อนเข้ารับการศึกษา:')),
                                  DataCell(Text('ม.6')),
                                ]),
                                DataRow(cells: [
                                  DataCell(Text('จบการศึกษาจาก:')),
                                  DataCell(Text('สระแก้ว')),
                                ]),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ])
            ],
          ),
        ),
      ),
    );
  }
}
