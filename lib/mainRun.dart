import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:flutter_application_1/historyme.dart';
import 'package:flutter_application_1/mainPage.dart';
import 'package:flutter_application_1/studytime.dart';

import 'login.dart';
import 'main.dart';
import 'search.dart';






void main() {
  runApp(App());
}

class App extends StatefulWidget {
  @override
  State<App> createState() => _App();
}

class _App extends State<App> {
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        theme: ThemeData(
          primarySwatch: Colors.grey,
        ),
        home: Login()
      ),
    );
  }
}
