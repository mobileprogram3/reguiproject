import 'package:flutter/material.dart';
import './drawer.dart';

// void main() {
//   runApp(MyApp());
// }

class MyApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

var body = ListView(

  children: [
    Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
                 
                
          ListTile(
            title: Text(
              'การยื่นคำร้องขอสำเร็จการศึกษา 2565 (ด่วน)',
              style: TextStyle(color: Color.fromARGB(255, 2, 5, 180)),
            ),
            
          ),
          Image.network('img/news1.png'),
          Padding(padding: EdgeInsets.all(3.0),),
          
          const Text("  ประกาศโดย   กองทะเบียนและประมวลผลการศึกษา   วันที่ประกาศ   14 ธันวาคม 2565",textAlign: TextAlign.left,style: TextStyle(fontSize: 13, color: Color.fromARGB(255, 122, 14, 6))),
          ButtonBar(
            alignment: MainAxisAlignment.center,
            // children: [
            //   TextButton(
            //     child: const Text(
            //       'คลิก',
            //       style: TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
            //     ),
            //     onPressed: () {},
            //   ),
            // ],
          ),
        ],
      ),
    ),
    Container(
      padding: EdgeInsets.all(15.0),
    ),
    Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          ListTile(
            title: const Text('กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย ปีการศึกษา 2565', style: TextStyle(color: Color.fromARGB(255, 2, 5, 180))),
           
          ),
          Image.network('img/news2.jpg'),
          Padding(padding: EdgeInsets.all(3.0),),
          const Text("  ประกาศโดย   กองทะเบียนและประมวลผลการศึกษา   วันที่ประกาศ   16 พฤศจิกายน 2564",textAlign: TextAlign.left,style: TextStyle(fontSize: 13 , color: Color.fromARGB(255, 122, 14, 6)) , ),
          ButtonBar(
            alignment: MainAxisAlignment.center,
            // children: [
            //   TextButton(
            //     child: const Text(
            //       'คลิก',
            //       style: TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
            //     ),
            //     onPressed: () {},
            //   ),
            // ],
          ),
        ],
      ),
    ),
    Container(
      padding: EdgeInsets.all(15.0),
    ),
    Card(
      // color: Colors.red,
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          ListTile(
            title: const Text('แบบประเมินความคิดเห็นต่อการให้บริการของสำนักงานอธิการบดี', style: TextStyle(color: Color.fromARGB(255, 2, 5, 180))),
            subtitle: Text(
              '       ขอเชิญนิสิตและผู้ปกครองร่วมทำแบบประเมินความคิดเห็นต่อการให้บริการของสำนักงานอธิการบดี',
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
          ),
          Image.network('img/news3.jpg'),
          Padding(padding: EdgeInsets.all(3.0),),
          const Text("  ประกาศโดย   กองทะเบียนและประมวลผลการศึกษา   วันที่ประกาศ   5 กุมภาพันธ์ 2564",textAlign: TextAlign.left,style: TextStyle(fontSize: 13, color: Color.fromARGB(255, 122, 14, 6) )),
          ButtonBar(
            alignment: MainAxisAlignment.center,
            // children: [
            //   TextButton(
            //     child: const Text(
            //       'คลิก',
            //       style: TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
            //     ),
            //     onPressed: () {},
            //   ),
            // ],
          ),
        ],
      ),
    ),
    Container(
      padding: EdgeInsets.all(15.0),
    ),
    Card(
      // color: Colors.red,
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          ListTile(
            title: const Text('LINE Official', style: TextStyle(color: Color.fromARGB(255, 2, 5, 180))),
            subtitle: Text(
              '       เพิ่มเพื่อน LINE Official ของกองทะเบียนฯ พิมพ์ "@regbuu" (ใส่ @ ด้วย) หรือ https://lin.ee/uNohJQP หรือสแกน Qrcode ด้านล่าง',
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
          ),
          Image.network('img/news4.jpg'),
          Padding(padding: EdgeInsets.all(3.0),),
          const Text("  ประกาศโดย   กองทะเบียนและประมวลผลการศึกษา   วันที่ประกาศ   13 เมษายน 2563",textAlign: TextAlign.left,style: TextStyle(fontSize: 13, color: Color.fromARGB(255, 122, 14, 6))),
          ButtonBar(
            alignment: MainAxisAlignment.center,
            // children: [
            //   TextButton(
            //     child: const Text(
            //       'คลิก',
            //       style: TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
            //     ),
            //     onPressed: () {},
            //   ),
            // ],
          ),
        ],
      ),
    ),
  ],
);

var appbar = AppBar(
  toolbarHeight: 110,
  leading: Builder(
    builder: (BuildContext context) {
      return IconButton(
        icon: const Icon(
          Icons.menu,
          color: Color.fromARGB(255, 0, 0, 0),
          // size: 40, // Changing Drawer Icon Size
        ),
        onPressed: () {
          Scaffold.of(context).openDrawer();
        },
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      );
    },
  ),
  title: Container(
      child: Row(
    children: [
      Column(
        children: [
          Container(
            margin: const EdgeInsets.all(5),
            width: 45,
            height: 45,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage('img/Buu-logo-black.png'),
              ),
            ),
          )
        ],
      ),
      Column(
        children: [
          Text(
            "มหาวิทยาลัยบูรพา",
            style: TextStyle(fontSize: 20, color: Colors.black),
          ),
          Text(
            "Burapha University",
            style: TextStyle(fontSize: 15, color: Colors.black),
          ),
        ],
      ),
    ],
  )),
  backgroundColor: Colors.yellow,
  elevation: 0.0,
  actions: <Widget>[
    IconButton(
        icon: Text(
          "ไทย",
          style: TextStyle(fontSize: 14, color: Colors.black),
        ),
        // color: Colors.black,
        onPressed: () {}),
    
  ],
);

class _MyStatefulWidgetState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(appBar: appbar, body: body, drawer: Drawerme()),
    );
  }
}